﻿using System;
using System.Collections.Generic;

namespace _7
{
    public interface ITrieTree : IReadOnlyCollection<string>, ICollection<string>
    {
        int GetCount(string word);
        bool Remove(string word, bool all);
    }
}
