﻿
namespace _7
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxWithTree = new System.Windows.Forms.GroupBox();
            this.treeView = new System.Windows.Forms.TreeView();
            this.openTrieTreeButton = new System.Windows.Forms.Button();
            this.removeWordButton = new System.Windows.Forms.Button();
            this.openTreeDialog = new System.Windows.Forms.OpenFileDialog();
            this.removeRuleTextBox = new System.Windows.Forms.TextBox();
            this.groupBoxWithTree.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxWithTree
            // 
            this.groupBoxWithTree.Controls.Add(this.treeView);
            this.groupBoxWithTree.Location = new System.Drawing.Point(4, 1);
            this.groupBoxWithTree.Name = "groupBoxWithTree";
            this.groupBoxWithTree.Size = new System.Drawing.Size(433, 188);
            this.groupBoxWithTree.TabIndex = 0;
            this.groupBoxWithTree.TabStop = false;
            this.groupBoxWithTree.Text = "Состояние дерева:";
            // 
            // treeView
            // 
            this.treeView.Location = new System.Drawing.Point(8, 22);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(417, 160);
            this.treeView.TabIndex = 0;
            // 
            // openTrieTreeButton
            // 
            this.openTrieTreeButton.Location = new System.Drawing.Point(4, 195);
            this.openTrieTreeButton.Name = "openTrieTreeButton";
            this.openTrieTreeButton.Size = new System.Drawing.Size(284, 23);
            this.openTrieTreeButton.TabIndex = 0;
            this.openTrieTreeButton.Text = "Открыть дерево";
            this.openTrieTreeButton.UseVisualStyleBackColor = true;
            this.openTrieTreeButton.Click += new System.EventHandler(this.ShowOpenTrieTreeDialog);
            // 
            // removeWordButton
            // 
            this.removeWordButton.Location = new System.Drawing.Point(298, 195);
            this.removeWordButton.Name = "removeWordButton";
            this.removeWordButton.Size = new System.Drawing.Size(139, 52);
            this.removeWordButton.TabIndex = 2;
            this.removeWordButton.Text = "Удалить больше заданной длины";
            this.removeWordButton.UseVisualStyleBackColor = true;
            this.removeWordButton.Click += new System.EventHandler(this.RemoveWords);
            // 
            // openTreeDialog
            // 
            this.openTreeDialog.Filter = "Текстовый файл (*.txt)|*.txt|Все файлы|*.*";
            this.openTreeDialog.Title = "Открыть файл";
            // 
            // removeRuleTextBox
            // 
            this.removeRuleTextBox.Location = new System.Drawing.Point(4, 224);
            this.removeRuleTextBox.Name = "removeRuleTextBox";
            this.removeRuleTextBox.Size = new System.Drawing.Size(284, 23);
            this.removeRuleTextBox.TabIndex = 3;
            this.removeRuleTextBox.Text = "0";
            this.removeRuleTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.DataValidating);
            this.removeRuleTextBox.Validated += new System.EventHandler(this.DataValidated);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(441, 259);
            this.Controls.Add(this.removeRuleTextBox);
            this.Controls.Add(this.removeWordButton);
            this.Controls.Add(this.openTrieTreeButton);
            this.Controls.Add(this.groupBoxWithTree);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Text = "Сильно ветвящиеся деревья - 7";
            this.groupBoxWithTree.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxWithTree;
        private System.Windows.Forms.Button openTrieTreeButton;
        private System.Windows.Forms.Button removeWordButton;
        private System.Windows.Forms.OpenFileDialog openTreeDialog;
        private System.Windows.Forms.TextBox removeRuleTextBox;
        private System.Windows.Forms.TreeView treeView;
    }
}