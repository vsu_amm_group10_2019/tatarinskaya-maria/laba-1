﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace _7
{
    public partial class TrieTree : ITrieTree
    {
        protected TrieTreeNode Root { get; set; }

        public int Count { get; protected set; }

        public bool IsReadOnly => false;

        public TrieTree()
            => Clear();

        protected TrieTreeNode GetLastSymbol(string word)
        {
            if (Root is not null)
            {
                TrieTreeNode current = Root;
                foreach (char symbol in word)
                {
                    if (current[symbol] is not null)
                        current = current[symbol];
                    else
                        return null;
                }
                return current;
            }
            return null;
        }

        protected bool RemoveFromNode(TrieTreeNode node, bool all)
        {
            if (node?.Count > 0)
            {
                int delete_count = (all ? node.Count : 1);
                node.Count -= delete_count;
                if (node.Count == 0)
                {
                    while (node?.Count == 0 && node.IsLeaf())
                    {
                        TrieTreeNode next = node.Parent;

                        foreach (KeyValuePair<char, TrieTreeNode> child in next)
                            if (child.Value == node)
                                next[child.Key] = null;
                        
                        node = next;
                    }

                    if (node is null)
                        Root = null;
                }
                Count -= delete_count;
                return true;
            }
            return false;
        }

        private static IEnumerable<string> GetRecursiveWords(TrieTreeNode node, StringBuilder word)
        {
            if (node is not null)
            {
                if (node.Count > 0)
                    yield return word.ToString();
                foreach (KeyValuePair<char, TrieTreeNode> pair in node)
                {
                    word.Append(pair.Key);
                    foreach (string result in GetRecursiveWords(pair.Value, word))
                        yield return result;
                    word.Remove(word.Length - 1, 1);
                }
            }
        }

        public void Add(string word)
        {
            if (Root is null)
                Root = new TrieTreeNode();

            int index = 0;
            TrieTreeNode current = Root;
            while (index < word.Length)
            {
                char symbol = word[index];
                if (current[symbol] is null)
                    current[symbol] = new TrieTreeNode(current);
                current = current[symbol];
                ++index;
            }

            ++current.Count;
            ++Count;
        }

        public void Clear()
        {
            Root = null;
            Count = 0;
        }

        public bool Contains(string word)
        {
            TrieTreeNode node = GetLastSymbol(word);
            if (node is not null)
                return node.Count > 0;
            return false;
        }

        public void CopyTo(string[] array, int arrayIndex)
        {
            if (array is null)
                throw new ArgumentNullException(nameof(array));

            if (arrayIndex < 0)
                throw new ArgumentOutOfRangeException(nameof(arrayIndex));

            using IEnumerator<string> words = GetEnumerator();
            while (words.MoveNext())
            {
                if (arrayIndex >= array.Length)
                    throw new ArgumentException("The number of elements in the source System.Collections.Generic.ICollection`1 is greater than the available space from arrayIndex to the end of the destination array.");
                
                array[arrayIndex++] = words.Current;
            }
        }

        public bool Remove(string word, bool all)
            => RemoveFromNode(GetLastSymbol(word), all);

        public bool Remove(string word)
            => Remove(word, false);

        public int GetCount(string word)
            => GetLastSymbol(word)?.Count ?? 0;

        public IEnumerator<string> GetEnumerator()
            => GetRecursiveWords(Root, new StringBuilder()).GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator()
            => GetEnumerator();
    }
}
