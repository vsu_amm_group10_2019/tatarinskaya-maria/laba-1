﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace _7
{
    public class TrieTreeAdapter : TrieTree
    {
        private readonly TreeView _treeView;

        public TrieTreeAdapter(TreeView treeView)
        {
            _treeView = treeView;
        }

        public void RemoveIfGreater(int symbolsCount)
        {
            var queue = new Queue<(int, TrieTreeNode)>();
            queue.Enqueue((0, Root));
            int newCount = 0;
            while (queue.Count > 0)
            {
                (int, TrieTreeNode) element = queue.Dequeue();
                newCount += element.Item2.Count;
                if (element.Item1 >= symbolsCount)
                    foreach (KeyValuePair<char, TrieTreeNode> pair in element.Item2)
                        element.Item2[pair.Key] = null;
                else
                    foreach (KeyValuePair<char, TrieTreeNode> pair in element.Item2)
                        queue.Enqueue((element.Item1 + 1, pair.Value));
            }
            Count = newCount;
        }

        private void CopyToView(TrieTreeNode trieNode, TreeNode viewNode)
        {
            var stack = new Stack<(TrieTreeNode, TreeNode)>();
            stack.Push((trieNode, viewNode));
            while (stack.Count > 0)
            {
                (TrieTreeNode, TreeNode) pair = stack.Pop();
                foreach (KeyValuePair<char, TrieTreeNode> node in pair.Item1)
                {
                    var newNode = new TreeNode(node.Key.ToString());
                    pair.Item2.Nodes.Add(newNode);
                    stack.Push((node.Value, newNode));
                }
            }
        }

        public void UpdateView()
        {
            _treeView.Nodes.Clear();
            if (Root is not null)
            {
                var viewNode = new TreeNode("");
                CopyToView(Root, viewNode);
                _treeView.Nodes.Add(viewNode);
            }
        }
    }
}
