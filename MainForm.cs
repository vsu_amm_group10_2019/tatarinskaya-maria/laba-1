﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;

namespace _7
{
    public partial class MainForm : Form
    {
        private int _max_word_number;
        private TrieTreeAdapter _last_trie_tree;

        public MainForm()
        {
            InitializeComponent();
            _max_word_number = 0;
            openTreeDialog.InitialDirectory = Environment.CurrentDirectory;
        }

        private void DataValidating(object sender, CancelEventArgs e)
        {
            var textBox = (TextBox)sender;
            e.Cancel = !int.TryParse(textBox.Text, out var number) || number < 0;
            if (e.Cancel)
            {
                textBox.Text = _max_word_number.ToString();
                MessageBox.Show(this, "Значение должно быть целым положительным числом.", "Произошла ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ShowOpenTrieTreeDialog(object sender, EventArgs e)
        {
            if (openTreeDialog.ShowDialog() == DialogResult.OK)
            {
                _last_trie_tree = new TrieTreeAdapter(treeView);
                using var fs = new FileStream(openTreeDialog.FileName, FileMode.Open, FileAccess.Read, FileShare.Read);
                using var sr = new StreamReader(fs);

                string line;
                while ((line = sr.ReadLine()) is not null)
                    _last_trie_tree.Add(line.Trim());

                _last_trie_tree.UpdateView();
            }
        }

        private void DataValidated(object sender, EventArgs e)
        {
            if (sender == removeRuleTextBox)
                _max_word_number = int.Parse(removeRuleTextBox.Text);
        }

        private void RemoveWords(object sender, EventArgs e)
        {
            _last_trie_tree.RemoveIfGreater(_max_word_number);
            _last_trie_tree.UpdateView();
        }
    }
}