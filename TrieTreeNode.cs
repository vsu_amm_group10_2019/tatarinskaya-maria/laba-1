﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace _7
{
    public partial class TrieTree
    {
        protected class TrieTreeNode : IEnumerable<KeyValuePair<char, TrieTreeNode>>
        {
            private readonly Dictionary<char, TrieTreeNode> _symbols;

            public TrieTreeNode Parent { get; set; }
            public int Count { get; set; }

            public TrieTreeNode(TrieTreeNode parent = null)
            {
                Parent = parent;
                Count = 0;
                _symbols = new Dictionary<char, TrieTreeNode>();
            }

            public bool IsLeaf()
            {
                foreach (KeyValuePair<char, TrieTreeNode> pair in _symbols)
                    if (pair.Value is not null)
                        return false;
                return true;
            }

            public TrieTreeNode this[char symbol]
            {
                get
                {
                    if (_symbols.ContainsKey(symbol))
                        return _symbols[symbol];
                    return null;
                }
                set
                {
                    if (_symbols.ContainsKey(symbol))
                        _symbols.Remove(symbol);
                    if (value is not null)
                        _symbols[symbol] = value;
                }
            }

            public IEnumerator<KeyValuePair<char, TrieTreeNode>> GetEnumerator()
                => _symbols.GetEnumerator();

            IEnumerator IEnumerable.GetEnumerator()
                => GetEnumerator();
        }
    }
}
